export const helpers = {
	methods: {

		generateProducts() {
			return Array.from(new Array(80), (x, i) => ({
				id: i + 1,
				name: `Name ${i + 1}`,
				city: this.getRandomInt(1, 5),
				category: this.getRandomInt(1, 5),
				price: this.getRandomInt(20, 200)
			}));
		},
		getRandomInt(min, max) {
			return Math.floor(min + Math.random() * (max + 1 - min));
		}
	}
};
